import httprequest from "../utils/httprequest";

//首页轮播图接口
export const indexImgs = (params) => httprequest.get("/indexImgs", params)
//首页数据接口
export const indexlist = (params) => httprequest.get("/prod/tagProdList", params)
//首页通知接口
export const indexnotice = (params) => httprequest.get("/shop/notice/noticeList", params)
//首页更多跳转
export const indexlastedprodpage = (params) => httprequest.get("/prod/prodListByTagId", params)
//首页通知跳转最新公告详情
export const indexinfo = (params) => httprequest.get(`/shop/notice/info/${params}`, params)
//首页同城帮帮
export const indexeveryday = (params) => httprequest.get("/prod/lastedProdPage", params)
//首页限时特惠
export const indexpreferential = (params) => httprequest.get("/prod/discountProdList", params)
//首页每日疯抢
export const indexmadgrab = (params) => httprequest.get("/prod/moreBuyProdList", params)
