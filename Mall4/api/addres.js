import httprequest from "../utils/httprequest";
//新增地址
export const addre = (params)=> httprequest.post('/p/address/addAddr',params)
//列表数据
export const addlist = (params)=> httprequest.get('/p/address/list',params)
//三级联动列表
export const linkage=(params)=>httprequest.get('/p/area/listByPid',params)


//编辑地址接口 
export const addrInfo=(params)=>httprequest.get(`/p/address/addrInfo/${params}`)
//编辑地址保存
export const updateAddr=(params)=>httprequest.put('/p/address/updateAddr',params)
//删除地址接口
export const deleteAddr=(params)=>httprequest.delete('/p/address/deleteAddr/326',params)
//默认地址接口
export const defaultAddr=(params)=>httprequest.put(`/p/address/defaultAddr/${params}`)