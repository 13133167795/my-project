import httprequest from "@/utils/httprequest";

// 购物车数据
export const shopList = (params) => httprequest.post("/p/shopCart/info", params)

// 购物车价格计算
export const calTotalPrice = (params) => httprequest.post("/p/shopCart/totalPay", params)

// 改变购物车数量接口
export const updateCount = (params) => httprequest.post("/p/shopCart/changeItem", params)

// 获取购物车数量
export const getCartCount = (params) => httprequest.get("/p/shopCart/prodCount", params)

// 删除购物车
export const DelBasket = (params) => httprequest.delete("/p/shopCart/deleteItem", params)